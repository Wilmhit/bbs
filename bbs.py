from random import randint, seed
from sympy import prime


def get_blum_number(seed_int):
    prime1 = 0
    prime2 = 0
    while ((prime1%4) != 3 and (prime2%4) != 3 or prime1 == prime2):
        prime1 = prime(randint(seed_int, seed_int*2))
        prime2 = prime(randint(seed_int, seed_int*2))
    print('Blum number generated')
    return  prime1 * prime2

def sequence_integer(seed):
    blum = get_blum_number(seed)
    x = seed
    for i in range(20000):
        x = (x * x) % blum
        yield x

def getLSB(integer):
    return bool(integer % 2)

def get_sequence():
    gen = sequence_integer(randint(1000, 10000))
    string = ''
    for x in gen:
        string += str(int(getLSB(x)))
    return string