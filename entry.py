from bbs import get_sequence
import tests

sequence = get_sequence()
print(sequence)
print('Bits test: ' + str(tests.test1(sequence)))
print('Series count test: ' + str(tests.test2(sequence)))
print('Long series test: ' + str(tests.test3(sequence)))
print('Poker test: ' + str(tests.test4(sequence)))