
# Counts 1s and 0s
def test1(sequence: str):
    total1 = 0
    total0 = 0
    for letter in sequence:
        if letter == '0':
            total0 += 1
        else:
            total1 += 1
    lenght = total1 + total0
    max = (lenght / 2) * 1.0275
    min = (lenght / 2) * 0.9725
    if  (min<total0<max) and (min<total1<max):
        return True
    return False

# Counts series and checks if distributed correctly
def test2(sequence: str):
    return check_dict(create_dict(sequence))


# Checks for longest series    
def test3(sequence: str):
    d = create_dict(sequence)
    max = 0
    for key in d:
        if key > max:
            max = key
    if max >= 26:
        return False
    return True

# Poker test
def test4(sequence: str):
    x = get_x(sequence)
    if x > 46.17 or x < 2.16:
        return False
    return True


def get_x(seq):
    # Create dictionary with counts
    gen = get_next_4_bits(seq)
    results = dict()
    for s in gen:
        results = add_to_dict(s, results)
    # Do math magic
    sum = 0
    for key in results:
        sum += results[key] * results[key]
    magic1 = 16/5000
    magic2 = 5000
    return (sum * magic1) - magic2

def get_next_4_bits(bits):
    to_ret = ''
    for bit in bits:
        if len(to_ret) == 4:
            yield to_ret
            to_ret = ''
        to_ret += bit


def create_dict(sequence):
    results = dict()
    previous = None
    series = 0
    for letter in sequence:
        if previous == letter or previous == None:
            series += 1
        else:
            results = add_to_dict(series, results)
            series = 1
        previous = letter
    return results


def check_dict(series: dict):
    for key in series:
        series[key] /= 2
    limits = {
        1: { 'min': 2315, 'max': 2685},
        2: { 'min': 1114, 'max': 1386},
        3: { 'min': 523, 'max': 723},
        4: { 'min': 240, 'max': 384},
        5: { 'min': 103, 'max': 209},
        'more': { 'min': 103, 'max': 209}
    }
    for key in limits:
        if key != 'more':
            if limits[key]['min'] > series[key] or limits[key]['max'] < series[key]:
                return False
    rest = 0
    ignore = [1,2,3,4,5]
    for key in series:
        if key not in ignore:
            rest += series[key]
    if limits['more']['min'] > rest or limits['more']['max'] < rest:
        return False
    return True


def add_to_dict(series, dictionary):
    if series not in dictionary:
        dictionary[series] = 1
    else:
        dictionary[series] += 1
    return dictionary

